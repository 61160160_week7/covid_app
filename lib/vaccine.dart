import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class VaccineWidget extends StatefulWidget {
  VaccineWidget({Key? key}) : super(key: key);

  @override
  _VaccineWidgetState createState() => _VaccineWidgetState();
}

class _VaccineWidgetState extends State<VaccineWidget> {
  var vaccines = ['-', '-', '-'];

  @override
  void initstate() {
    super.initState();
    _loadVaccines();
  }

  _loadVaccines() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      vaccines = prefs.getStringList('vaccines') ?? ['-', '-', '-'];
    });
  }

  _saveloadVaccines() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setStringList('vaccines', vaccines);
    });
  }

  Widget _vaccineComboBox(
      {required String title,
      required String value,
      ValueChanged<String?>? onChanged}) {
    return Container(
        padding: EdgeInsets.all(16.0),
        child: Row(
          children: [
            Text(title),
            Expanded(
              child: Container(),
            ),
            DropdownButton(items: [
              DropdownMenuItem(child: Text('-'), value: '-'),
              DropdownMenuItem(child: Text('Prizer'), value: 'Prizer'),
              DropdownMenuItem(
                  child: Text('Jonhson & Jonhson'), value: 'Jonhson & Jonhson'),
              DropdownMenuItem(
                  child: Text('AstraZeneca'), value: 'AstraZeneca'),
              DropdownMenuItem(child: Text('Novavax'), value: 'Novavax'),
              DropdownMenuItem(child: Text('Sinopharm'), value: 'Sinopharm'),
              DropdownMenuItem(child: Text('Sinovac'), value: 'Sinovac'),
            ], value: value, onChanged: onChanged),
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Vaccine')),
      body: ListView(
        children: [
          _vaccineComboBox(
              title: 'เข็ม 1',
              value: vaccines[0],
              onChanged: (newValue) {
                setState(() {
                  vaccines[0] = newValue!;
                });
              }),
          _vaccineComboBox(
              title: 'เข็ม 2',
              value: vaccines[1],
              onChanged: (newValue) {
                setState(() {
                  vaccines[1] = newValue!;
                });
              }),
          _vaccineComboBox(
              title: 'เข็ม 3',
              value: vaccines[2],
              onChanged: (newValue) {
                setState(() {
                  vaccines[2] = newValue!;
                });
              }),
          ElevatedButton(
              onPressed: () {
                _saveloadVaccines();
                Navigator.pop(context);
              },
              child: Text('Save'))
        ],
      ),
    );
  }
}
