import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RiskWidget extends StatefulWidget {
  RiskWidget({Key? key}) : super(key: key);

  @override
  _RiskWidgetState createState() => _RiskWidgetState();
}

class _RiskWidgetState extends State<RiskWidget> {
  var riskValues = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
  ];
  var risk = [
    'มีไข้หรือหนาวสั่น',
    'มีอาการไอ',
    'มีอาการแน่นหน้าอก',
    'มีอาการเหนื่อยล้า',
    'ปวดกล้ามเนื้อหรือร่างกาย',
    'ปวดหัว',
    'ไม่ได้กลิ่นและรส',
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Risk')),
        body: ListView(
          children: [
            CheckboxListTile(
                value: riskValues[0],
                title: Text(risk[0]),
                onChanged: (newValue) {
                  setState(() {
                    riskValues[0] = newValue!;
                  });
                }),
            CheckboxListTile(
                value: riskValues[1],
                title: Text(risk[1]),
                onChanged: (newValue) {
                  setState(() {
                    riskValues[1] = newValue!;
                  });
                }),
            CheckboxListTile(
                value: riskValues[2],
                title: Text(risk[2]),
                onChanged: (newValue) {
                  setState(() {
                    riskValues[2] = newValue!;
                  });
                }),
            CheckboxListTile(
                value: riskValues[3],
                title: Text(risk[3]),
                onChanged: (newValue) {
                  setState(() {
                    riskValues[3] = newValue!;
                  });
                }),
            CheckboxListTile(
                value: riskValues[4],
                title: Text(risk[4]),
                onChanged: (newValue) {
                  setState(() {
                    riskValues[4] = newValue!;
                  });
                }),
            CheckboxListTile(
                value: riskValues[5],
                title: Text(risk[5]),
                onChanged: (newValue) {
                  setState(() {
                    riskValues[5] = newValue!;
                  });
                }),
            CheckboxListTile(
                value: riskValues[6],
                title: Text(risk[6]),
                onChanged: (newValue) {
                  setState(() {
                    riskValues[6] = newValue!;
                  });
                }),
            ElevatedButton(
                onPressed: () async {
                  await _saveRisk();
                  Navigator.pop(context);
                },
                child: const Text('save'))
          ],
        ));
  }

  @override
  void initState() {
    super.initState();
    _loadRisk();
  }

  Future<void> _loadRisk() async {
    var prefs = await SharedPreferences.getInstance();
    var strriskValue = prefs.getString('risk_values') ??
        '[false, false, false, false, false, false, false]';
    // print(strQuestionValue.substring(1, strQuestionValue.length - 1));
    var arrStrriskValues =
        strriskValue.substring(1, strriskValue.length - 1).split(',');
        print(strriskValue.length);
    setState(() {
      for (var i = 0; i < arrStrriskValues.length; i++) {
        riskValues[i] = (arrStrriskValues[i].trim() == 'true');
      }
    });
  }

  Future<void> _saveRisk() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('risk_values', riskValues.toString());
  }
}
